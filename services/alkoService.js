const request = require("request")
const axios = require('axios')
var jsdom = require("jsdom")
const { JSDOM } = jsdom
const Source = require('../models/source')
const BeerInstance = require('../models/beerInstance')
const Beer = require('../models/beer')
const Brewery = require('../models/brewery')
const PendingBeer = require('../models/pendingBeer')

//const checkBeerExistence = async function (source, name, brewery) {
const checkBeerExistence = async function (source, name) {

  // TODO: find out how to check brewery too
  let instances = BeerInstance.find({ sourceFullName: name, source: source })
  console.log('fetched beer instances... ', instances.length)
  // TODO: check for price changes, size etc...

  if (instances.length === undefined) {
    return false
  } else {
    return true
  }
}

const createBeerInstance = async function (source, name, breweryName) {
  console.log('starting createBeerInstance')
  console.log('name: ', name)
  // first check if a beer exists, if not then create beer first
  //let beer = await Beer.find({ name: name, brewery: breweryName })
  //  .populate('beerInstance')

  // first try to find a brewery, if no brewery is found then create a pending approval
  // var firstWord = name.split(' ').slice(0,1).join(' ')
  // var firstTwoWords = name.split(' ').slice(0,2).join(' ')
  // var firstThreeWords = name.split(' ').slice(0,3).join(' ')
  // console.log('1st: ', firstWord)
  // console.log('2nd: ', firstTwoWords)
  // console.log('3rd: ', firstThreeWords)

  let beer = await BeerInstance.find({ sourceFullName: name })
    .populate('beer')

  console.log('fetched beerinstance:', beer)

  // if no beer is found by name nor additionalName, fetch brewery
  if (!beer.length) {
    /*     console.log('no beer found, fetching brewery name first')
    let brewery = await Brewery.find({ name: firstThreeWords })
    if (brewery === undefined) {
      brewery = await Brewery.find({ name: firstTwoWords })
    }
    if (brewery === undefined) {
      brewery = await Brewery.find({ name: firstWord })
    } */

    // tried to find brewery by first three words
    // console.log('brewery found: ', brewery)

    // if (!brewery.length || brewery.length > 1) {
    // console.log('no brewery / multiple breweries found, creating a pending beer')
    // var newBrewery = new Brewery({
    //   name: breweryName
    //})

    var sourceId = await findSource({ name: 'Alko' })
    //brewery = await newBrewery.save()
    let pendingBeer = new PendingBeer({
      name: name,
      source: sourceId
    })
    const savedPendingBeer = await pendingBeer.save()
    // } else {
    //   // else create a new beer for the brewery in question
    //   let beer = new Beer({
    //     name: name,
    //     brewery: brewery[0]._id
    //   })

    //   const savedBeer = await beer.save()
    // }
  }

  //let instance = BeerInstance({ name: name })
  // then create a beer instance
}

const findSource = async function (params) {
  try {
    return await Source.findOne(params)
  } catch(err) { console.log(err) }
}

const getAlkoBeers = async () => {
  console.log('started getAlkoBeers')
  try {
    return await axios.get('https://www.alko.fi/tuotteet/tuotelistaus?SearchTerm=*&PageSize=12&SortingAttribute=&PageNumber=2&SearchParameter=%26%40QueryTerm%3D*%26ContextCategoryUUID%3DyW3AqHh4ul0AAAFVIGocppid%26OnlineFlag%3D1')
  } catch (error) {
    console.error(error)
  }
}

const getAll = async () => {
  var sourceId = await findSource({ name: 'Alko' })
  console.log('found source', sourceId)
  console.log ('start getAll axios')
  const resp = await getAlkoBeers()

  const dom = new JSDOM(resp.data)
  console.log(dom)

  var products = dom.window.document.querySelectorAll('.mini-card')
  var productLinks = dom.window.document.querySelectorAll('.js-product-link')
  //var prices = dom.window.document.querySelectorAll('.js-product-link>.price-wrapper')
  let pricez = dom.window.document.querySelectorAll('.price-wrapper')
  var prices = Array.from(pricez).filter((x, i) => i % 2)
  console.log(prices.length)

  console.log('starting to loop through beers')
  for (var i = 0; i < 1; i++) {
    console.log(i)
    const name = productLinks[i]['title']
    console.log(productLinks[i]['title'])
    //console.log(products[0].title)
    //console.log(prices[0].getAttribute('content'))
    console.log(prices[i].getAttribute('content'))
    let existing = await checkBeerExistence(sourceId, productLinks[i]['title'])
    console.log('checked existence...', existing)

    if (existing === true) {
      console.log('found existing instance from Alko')
    }

    if (existing === false) {
      console.log('foo')
      const created = await createBeerInstance('Alko', name)
      console.log('created pending beer...')
    } else {
      console.log('bar')
    }
    //var kalijat = dom.window.document.querySelectorAll('.js-product-link').forEach(el => console.log(el.dataset.productPageUrl))
    //var kalijat2 = dom.window.document.querySelectorAll('.js-product-link')
    //console.log(kalijat2[0].dataset.productPageUrl)
  }
}

//exports.getAll = function alkoService() {
const getAll2 = async () => {
  //var sourceId = Source.findOne({ name: 'Alko' })
  var sourceId = await findSource({ name: 'Alko' })
  console.log('found source', sourceId)
  console.log ('start getAll')
  request("https://www.alko.fi/tuotteet/tuotelistaus?SearchTerm=*&PageSize=12&SortingAttribute=&PageNumber=2&SearchParameter=%26%40QueryTerm%3D*%26ContextCategoryUUID%3DyW3AqHh4ul0AAAFVIGocppid%26OnlineFlag%3D1", (error, response, body) => {

    console.log('starting alkoService')

    var sourceId = findSource({ name: 'Alko' })

    const dom = new JSDOM(body)

    var products = dom.window.document.querySelectorAll('.mini-card')
    var productLinks = dom.window.document.querySelectorAll('.js-product-link')
    //var prices = dom.window.document.querySelectorAll('.js-product-link>.price-wrapper')
    let pricez = dom.window.document.querySelectorAll('.price-wrapper')
    var prices = Array.from(pricez).filter((x, i) => i % 2)
    console.log(prices.length)

    // for (var i = 0; i < beerz.length; i++) {
    console.log('starting to loop through beers')
    for (var i = 0; i < 10; i++) {
      console.log(i)
      console.log(productLinks[i]['title'])
      //console.log(products[0].title)
      //console.log(prices[0].getAttribute('content'))
      console.log(prices[i].getAttribute('content'))
      let existing = checkBeerExistence(sourceId, productLinks[i]['title'])
      console.log('checked existence...', existing)

      if (existing === false) {
        console.log('foo')
      } else {
        console.log('bar')
      }
      //var kalijat = dom.window.document.querySelectorAll('.js-product-link').forEach(el => console.log(el.dataset.productPageUrl))
      //var kalijat2 = dom.window.document.querySelectorAll('.js-product-link')
      //console.log(kalijat2[0].dataset.productPageUrl)
    }
  })
}

module.exports = {
  getAll
}