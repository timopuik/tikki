const mongoose = require('mongoose')

const ratebeerDataSchema = new mongoose.Schema({
  average: Number,
  name: String,
  style: String,
  data_updated: Date,
  style_points: Number,
  overall_points: Number,
  rating: { type: mongoose.Schema.Types.ObjectId, ref: 'Rating' },
  // TODO: there can be several breweries
})

ratebeerDataSchema.statics.format = (ratebeerData) => {
  return {
    id: ratebeerData._id,
    average: ratebeerData.average,
    name: ratebeerData.name,
    style: ratebeerData.style,
    style_points: ratebeerData.style_points,
    overall_points: ratebeerData.overall_points
  }
}

const RatebeerData = mongoose.model('RatebeerData', ratebeerDataSchema)

module.exports = RatebeerData