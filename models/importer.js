const mongoose = require('mongoose')

const importerSchema = new mongoose.Schema({
  name: String,
  country: String,
})

importerSchema.statics.format = (importer) => {
  return {
    id: importer._id,
    name: importer.name,
    country: importer.country,
  }
}

const Importer = mongoose.model('Importer', importerSchema)

module.exports = Importer