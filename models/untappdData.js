const mongoose = require('mongoose')

const untappdDataSchema = new mongoose.Schema({
  average: Number,
  name: String,
  style: String,
  data_updated: Date,
})

untappdDataSchema.statics.format = (untappdData) => {
  return {
    id: untappdData._id,
    name: untappdData.name,
    style: untappdData.style,
  }
}

const UntappdData = mongoose.model('UntappdData', untappdDataSchema)

module.exports = UntappdData