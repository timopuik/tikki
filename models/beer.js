const mongoose = require('mongoose')

const beerSchema = new mongoose.Schema({
  name: String,
  style: String,
  additionalNames: [String],
  //ut_data: { type: mongoose.Schema.Types.ObjectId, ref: 'UntappdData' },
  //rb_data: { type: mongoose.Schema.Types.ObjectId, ref: 'RatebeerData' },
  brewery: { type: mongoose.Schema.Types.ObjectId, ref: 'Brewery' },
  instances: [{ type: mongoose.Schema.Types.ObjectId, ref: 'BeerInstance' }]
  // TODO: there can be several breweries
})

beerSchema.statics.format = (beer) => {
  return {
    id: beer._id,
    name: beer.name,
    style: beer.style,
    additionalNames: beer.additionalNames,
    brewery: beer.brewery,
    instances: beer.instances
  }
}

const Beer = mongoose.model('Beer', beerSchema)

module.exports = Beer