const mongoose = require('mongoose')

const ratingSchema = new mongoose.Schema({
  untappd_id: Number,
  rb_id: String,
  ut_updated_at: Date,
  rb_updated_at: Date,
  ut_rating_count: Number,
  ut_average: Number,
  rb_rating_count: Number,
  rb_average: Number,
  rb_overall: Number,
  rb_style: Number
})

ratingSchema.statics.format = (rating) => {
  return {
    id: rating.id,
    untappd_id: rating.untappd_id,
    rb_id: rating.rb_id,
    ut_updated_at: rating.ut_updated_at,
    rb_updated_at: rating.rb_updated_at,
    ut_rating_count: rating.ut_rating_count,
    ut_average: rating.ut_average,
    rb_rating_count: rating.rb_rating_count,
    rb_average: rating.rb_average,
    rb_overall: rating.rb_overall,
    rb_style: rating.rb_style
  }
}

const Rating = mongoose.model('Rating', ratingSchema)

module.exports = Rating