const mongoose = require('mongoose')

const pendingBeerSchema = new mongoose.Schema({
  name: String,
  source: { type: mongoose.Schema.Types.ObjectId, ref: 'Source' },
})

pendingBeerSchema.statics.format = (pendingBeer) => {
  return {
    id: pendingBeer._id,
    name: pendingBeer.name,
    source: pendingBeer.source
  }
}

const PendingBeer = mongoose.model('PendingBeer', pendingBeerSchema)

module.exports = PendingBeer