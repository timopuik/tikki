const mongoose = require('mongoose')

const brewerySchema = new mongoose.Schema({
  name: String,
  beers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Brewery' }]
})

brewerySchema.statics.format = (brewery) => {
  return {
    id: brewery._id,
    name: brewery.name,
    beers: brewery.beers
  }
}

const Brewery = mongoose.model('Brewery', brewerySchema)

module.exports = Brewery