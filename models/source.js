const mongoose = require('mongoose')

const sourceSchema = new mongoose.Schema({
  name: String,
  shopUrl: String,
  source_url: String,
  // TODO: there can be several breweries
})

sourceSchema.statics.format = (source) => {
  return {
    id: source._id,
    name: source.name,
    shopUrl: source.shopUrl,
  }
}

const Source = mongoose.model('Source', sourceSchema)

module.exports = Source