const mongoose = require('mongoose')

const beerInstanceSchema = new mongoose.Schema({
  beer: { type: mongoose.Schema.Types.ObjectId, ref: 'Beer' },
  source: { type: mongoose.Schema.Types.ObjectId, ref: 'Source' },
  price: Number,
  size: Number,
  external_id: String,
  url: String,
  added_on: Date,
  removed_on: Date,
  sourceFullName: String,
})

beerInstanceSchema.statics.format = (beerInstance) => {
  return {
    id: beerInstance._id,
    name: beerInstance.name,
    style: beerInstance.style,
    sourceFullName: beerInstance.sourceFullName,
  }
}

const BeerInstance = mongoose.model('BeerInstance', beerInstanceSchema)

module.exports = BeerInstance