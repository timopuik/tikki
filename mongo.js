const mongoose = require('mongoose')

const url = 'mongodb://tikkiuser:CX6KFxBuAALw4kdL@ds123434.mlab.com:23434/heroku_x3r6kx87'

mongoose.connect(url)
mongoose.Promise = global.Promise

const Beer = mongoose.model('Beer', {
  _id: Number,
  name: String,
  brewery: String
})

Beer
  .find({})
  .then(result => {
    result.forEach(beer => {
      console.log(beer)
    })
    mongoose.connection.close()
  })

const Rating = mongoose.model('Rating', {
  _id: Number,
  untappd_id: Number,
  rb_id: String,
  ut_updated_at: Date,
  rb_updated_at: Date,
  ut_rating_count: Number,
  ut_average: Number,
  rb_rating_count: Number,
  rb_average: Number,
  rb_overall: Number,
  rb_style: Number
})

Rating
  .find({})
  .then(result => {
    result.forEach(rating => {
      console.log(rating)
    })
    mongoose.connection.close()
  })