const http = require('http')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const middleware = require('./utils/middleware')
const beersRouter = require('./controllers/beers')
const usersRouter = require('./controllers/users')
const loginRouter = require('./controllers/login')
const sourcesRouter = require('./controllers/sources')
const pendingBeersRouter = require('./controllers/pendingBeers')
const breweriesRouter = require('./controllers/breweries')
const config = require('./utils/config')
const alkoService = require('./services/alkoService')

mongoose.connect(config.mongoUrl)
mongoose.Promise = global.Promise

app.use(cors())
app.use(bodyParser.json())
app.use(express.static('build'))
app.use(middleware.logger)

app.use('/api/beers', beersRouter)
app.use('/api/users', usersRouter)
app.use('/api/login', loginRouter)
app.use('/api/sources', sourcesRouter)
app.use('/api/pendingBeers', pendingBeersRouter)
app.use('/api/breweries', breweriesRouter)

app.use(middleware.error)

const server = http.createServer(app)

server.listen(config.port, () => {
  console.log(`server running on port ${config.port}`)
})

server.on('close', () => {
  mongoose.connection.close()
})

module.exports = {
  app, server
}