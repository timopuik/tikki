const jwt = require('jsonwebtoken')
const beersRouter = require('express').Router()
const Beer = require('../models/beer')
const Brewery = require('../models/brewery')
const alko = require('../services/alkoService')
const BeerInstance = require('../models/beerInstance')
const Source = require('../models/source')

const getTokenFrom = (request) => {
  const authorization = request.get('authorization')
  if (authorization && authorization.toLowerCase().startsWith('bearer')) {
    return authorization.substring(7)
  }
  return null
}

beersRouter.get('/', async (request, response) => {
  console.log('beersourter')
  const beers = await Beer
    .find({})
    .populate('brewery')
    .populate('beerInstance')

  console.log(beers.map(Beer.format))

  response.json(beers.map(Beer.format))
})

beersRouter.get('/:id', async (request, response) => {
  console.log('get id')

  try {
    console.log('get id')

    const beer = await Beer.findById(request.params.id)

    if (beer) {
      response.json(Beer.format(beer))
    } else {
      response.status(404).end()
    }
  } catch (exception) {
    console.log(exception)
    response.status(400).send({ error: 'malformatted id' })
  }
})

beersRouter.put('/', async (request, response) => {
  console.log('get fetch blaa')
  alko.getAll()
})

beersRouter.post('/', async (request, response) => {
  const body = request.body
  try {
    //const token = getTokenFrom(request)
    //const decodedToken = jwt.verify(token, config.SECRET)

    //if (!token || !decodedToken.id) {
    //  return response.status(401).json({ error: 'token missing or invalid' })
    //}

    //if (body.content === undefined) {
    //  return response.status(400).json({ error: 'content missing' })
    //}

    //const user = await User.findById(decodedToken.id)
    //if (!user) {

    //}
    // TODO: there can be many breweries
    console.log(body.breweryName)

    let brewery = await Brewery.find({ name: body.breweryName })
    console.log(brewery)

    if (!brewery.length) {
      console.log("no brewery found")
      var newBrewery = new Brewery({
        name: body.breweryName
      })

      brewery = await newBrewery.save()
    }
    console.log(brewery)

    const beer = new Beer({
      name: body.beerName,
      style: body.beerStyle,
      brewery: brewery === undefined ? savedBrewery._id : brewery._id
    })

    const savedBeer = await beer.save()

    if (brewery.beers === undefined) {
      brewery.beers = []
    }
    brewery.beers = brewery.beers.concat(savedBeer._id)
    const savedbr = await brewery.save(function(){})

    response.json(Beer.format(beer))
  } catch(exception) {
    if (exception.name === 'JsonWebTokenError') {
      response.status(401).json({ error: exception.message })
    } else {
      console.log(exception)
      response.status(500).json({ error: 'something went wrong...' })
    }
  }
})

beersRouter.post('/:id/instance', async (request, response) => {
  const body = request.body
  try {
    console.log("POST create beerinstance")


    let beerInstance

    // let beerInstance = await BeerInstance.find({ name: body.beerName, source: body.sourceName })
    console.log("lol")
    let source = await Source.find({ name: body.sourceName })
    let beer = await Beer.findById( body.beerId )

    console.log(source)
    console.log(beer)

    if (beerInstance === undefined) {
      console.log("no beer instance found")
      var newBeerInstance = new BeerInstance({
        source: source._id,
        price: body.price,
        size: body.size,
        url: body.url
      })

      const savedBeerInstance = await newBeerInstance.save()
      console.log("fartta")
      console.log(beer)

      if (beer.instances === undefined) {
        beer.instances = []
      }

      //Beer.findOneAndUpdate( { _id: body.beerId }, { $push: { instances: savedBeerInstance._id } }, { upsert: true, new: true } )


      beer.instances.push(savedBeerInstance._id)
      const savedBeer = await beer.save()
    }

    // let oof = Beer.findOne({ _id: body.beerId })

    // console.log(oof)

    console.log("beerinstance created, beer saved")

    response.json(Beer.format(beer))
  } catch(exception) {
    if (exception.name === 'JsonWebTokenError') {
      response.status(401).json({ error: exception.message })
    } else {
      console.log(exception)
      response.status(500).json({ error: 'something went wrong...' })
    }
  }
})


module.exports = beersRouter