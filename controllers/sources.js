const jwt = require('jsonwebtoken')
const Source = require('../models/source')
const sourcesRouter = require('express').Router()

sourcesRouter.get('/', async (request, response) => {
    console.log("GET fetch sources")
    const beers = await Beer
    .find({})
    .populate('brewery')
    .populate('beerInstance')
  
    console.log(beers.map(Beer.format))
  
    response.json(beers.map(Beer.format))
  })

sourcesRouter.get('/:id', async (request, response) => {
    console.log("GET fetch source id")
    const source = await Source
    .find({})
    .populate('brewery')
    .populate('beerInstance')
  
    console.log(beers.map(Beer.format))
  
    response.json(beers.map(Beer.format))
  })

sourcesRouter.post('/', async (request, response) => {
    console.log("POST create source")

    const body = request.body
    try {
      const source = new Source({
        name: body.sourceName,
        shopUrl: body.shopUrl,
      })

      const savedSource = await source.save()
      // console.log(sources.map(Source.format))
      response.json(Source.format(source))
    } catch(exception) {
        if (exception.name === 'JsonWebTokenError') {
          response.status(401).json({ error: exception.message })
        } else {
          console.log(exception)
          response.status(500).json({ error: 'something went wrong...' })
        }
      }
  })

  module.exports = sourcesRouter