const jwt = require('jsonwebtoken')
const pendingBeersRouter = require('express').Router()
const Beer = require('../models/beer')
const Brewery = require('../models/brewery')
const BeerInstance = require('../models/beerInstance')
const Source = require('../models/source')
const PendingBeer = require('../models/pendingBeer')

const getTokenFrom = (request) => {
  const authorization = request.get('authorization')
  if (authorization && authorization.toLowerCase().startsWith('bearer')) {
    return authorization.substring(7)
  }
  return null
}

pendingBeersRouter.get('/', async (request, response) => {
  console.log('pending beers router')
  const pendingBeers = await PendingBeer
    .find({})
    .populate('source')


  console.log(pendingBeers.map(PendingBeer.format))

  response.json(pendingBeers.map(PendingBeer.format))
})

pendingBeersRouter.get('/:id', async (request, response) => {

  try {
    const pendingBeer = await PendingBeer.findById(request.params.id)
      .populate('source')

    if (pendingBeer) {
      console.log('get pending id... ', pendingBeer)
      response.json(PendingBeer.format(pendingBeer))
    } else {
      response.status(404).end()
    }
  } catch (exception) {
    console.log(exception)
    response.status(400).send({ error: 'malformatted id' })
  }
})


pendingBeersRouter.post('/', async (request, response) => {
  const body = request.body

  try {
    console.log('printing post pendingbeer body... ', body.beerData.breweryName)

    // change this to dynamic parameter after other integrations have been done
    let source = await Source.findOne({ name: 'Alko' })
    let brewery = await Brewery.findOne({ name: body.beerData.breweryName })
    console.log('pendingbeers printing brewery', brewery)
    console.log('pendingbeers printing brewery id', brewery._id)
    console.log('pendingbeers printing body...', body.beerData)

    const beer = new Beer({
      name: body.beerData.beerName,
      // style: body.beerStyle,
      brewery: brewery._id
    })

    const savedBeer = await beer.save()

    if (brewery.beers === undefined || brewery.beers.length < 1) {
      console.log('brewery.beers is empty')
      brewery.beers = []
    }

    brewery.beers = brewery.beers.concat(savedBeer._id)
    console.log('printing beers after concat: ', brewery.beers)

    const savedbr = await brewery.save(function(){})

    const pendingBeer = await PendingBeer.findById(body.beerData.pendingBeerId)

    console.log('printing fetched pendingbeer... ', pendingBeer)

    // after successful brewery save, create instance
    const newBeerInstance = new BeerInstance({
      source: source._id,
      sourceFullName: pendingBeer.name
    })

    const savedBeerInstance = await newBeerInstance.save()
    console.log('printing savedbeerinstance... ', savedBeerInstance)

    if (savedBeer.instances === undefined || savedBeer.instances.length < 1 ) {
      console.log('savedbeer.instances is empty')
      savedBeer.instances = []
    }

    savedBeer.instances = savedBeer.instances.concat(savedBeerInstance._id)

    let savedB = await savedBeer.save()

    let result = await PendingBeer.findByIdAndDelete(pendingBeer._id)

    response.json(Beer.format(beer))

  } catch(exception) {
    if (exception.name === 'JsonWebTokenError') {
      response.status(401).json({ error: exception.message })
    } else {
      console.log(exception)
      response.status(500).json({ error: 'something went wrong...' })
    }
  }
})

module.exports = pendingBeersRouter