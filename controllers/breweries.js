const jwt = require('jsonwebtoken')
const breweriesRouter = require('express').Router()
const Brewery = require('../models/brewery')

breweriesRouter.get('/', async (request, response) => {
  console.log('get all breweries')
  const breweries = await Brewery
    .find({})

  console.log(breweries.map(Brewery.format))

  response.json(breweries.map(Brewery.format))
})

breweriesRouter.post('/', async (request, response) => {
  const body = request.body
  console.log('ululu', body.content.breweryName)
  try {
    // TODO: there can be many breweries
    console.log('create brewery by name: ', body.content.breweryName)

    let brewery = await Brewery.find({ name: body.content.breweryName })

    console.log(brewery)

    if (!brewery.length) {
      console.log('no brewery found')
      var newBrewery = new Brewery({
        name: body.content.breweryName
      })

      brewery = await newBrewery.save()
    }
    console.log(brewery)

    response.json(Brewery.format(brewery))
  } catch(exception) {
    if (exception.name === 'JsonWebTokenError') {
      response.status(401).json({ error: exception.message })
    } else {
      console.log(exception)
      response.status(500).json({ error: 'something went wrong...' })
    }
  }
})

module.exports = breweriesRouter