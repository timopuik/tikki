POST http://localhost:3001/api/beers/5c3b369fbb48a424c4d28cd0/instance
content-type: application/json

{
    "sourceName": "Alko",
    "beerName": "bar",
    "beerId": "5c6d58597961ae47441c9ecf",
    "price": "5.99",
    "size": "0.33"
}